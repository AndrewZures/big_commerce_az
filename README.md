# Big Commerce Code Assignment

application is live at https://big-commerce-az.herokuapp.com/

##### Specs #####
run all specs with the `rspec` command

##### Design #####

I used standard rails controller, model, and view logic for this assignment because it provided the easiest implementation of the requirements.  

`admin` and `api` controllers are namespaces and call into `managers` for the data they would like.  

I used the manager pattern to allow for the model layer to have only one responsibility - managing database interactions.  The manager layer is responsible for interacting with model as well as coordinating any other events related to app interactions.  For example, if we were to send emails after a product creation, the `ProductManager` would be the layer that coordinates the saving of the product data, then the sending of the email.  In the current assignment this layer doesn't do much since the assignment is almost entirely CRUD interactions.  
