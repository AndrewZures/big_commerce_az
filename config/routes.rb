Rails.application.routes.draw do
  namespace :admin do
    resources :products, except: [:destroy] do
      resources :rate_plans, except: [:destroy]
    end
  end

  namespace :api do
    resources :products, only: [:index, :show]
  end

  root 'admin/products#index'
end
