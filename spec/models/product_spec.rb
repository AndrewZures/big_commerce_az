describe Product, type: :model do
  subject { described_class }

  describe '#editable_fields' do
    it 'returns a list of editable fields for the model' do
      expect(subject.editable_fields).to include(
        :title,
        :description,
        :start_date,
        :end_date,
        :status
      )
    end
  end

  describe '#sync_fields' do
    it 'changes status to discontinued if new end date is present and is in past' do
      product = subject.create!(
        title: 'New Title',
        description: 'New Description',
        end_date: nil,
        start_date: DateTime.now
      )

      expect(product.status).to eq(Product::Status::ACTIVE)
      product.update!(end_date: 3.days.ago)
      product.reload
      expect(product.status).to eq(Product::Status::DISCONTINUED)
    end

    it 'does not change status to discontinued if new end date is present but date is in future' do
      product = subject.create!(
        title: 'New Title',
        description: 'New Description',
        end_date: nil,
        start_date: DateTime.now
      )

      expect(product.status).to eq(Product::Status::ACTIVE)
      product.update!(end_date: 3.days.from_now)
      product.reload
      expect(product.status).to eq(Product::Status::ACTIVE)
    end

    it 'changes end_date to DateTime.now if status has changed to discontinued' do
      product = subject.create!(
        title: 'New Title',
        description: 'New Description',
        end_date: nil,
        start_date: DateTime.now
      )

      expect(product.end_date).to be_nil
      product.update!(status: Product::Status::DISCONTINUED)
      product.reload
      expect(product.status).to eq(Product::Status::DISCONTINUED)
      expect(product.end_date).to_not be_nil
    end
  end
end
