describe RatePlan, type: :model do
  subject { described_class }
  let!(:product) { create(:product) }

  describe '#editable_fields' do
    it 'returns a list of editable fields for the model' do
      expect(subject.editable_fields).to include(
        :title,
        :description,
        :start_date,
        :end_date,
        :recurrence,
        :price,
        :status,
        :product_id
      )
    end
  end

  describe '#sync_fields' do
    it 'changes status to retired if new end date is present and is in past' do
      rate_plan = subject.create!(
        title: 'New Title',
        description: 'New Description',
        end_date: nil,
        start_date: DateTime.now,
        recurrence: :one_time,
        product_id: product.id
      )

      expect(rate_plan.status).to eq(RatePlan::Status::ACTIVE)
      rate_plan.update!(end_date: 3.days.ago)
      rate_plan.reload
      expect(rate_plan.status).to eq(RatePlan::Status::RETIRED)
    end

    it 'does not change status to retired if new end date is present but date is in future' do
      rate_plan = subject.create!(
        title: 'New Title',
        description: 'New Description',
        end_date: nil,
        start_date: DateTime.now,
        recurrence: :one_time,
        product_id: product.id
      )

      expect(rate_plan.status).to eq(RatePlan::Status::ACTIVE)
      rate_plan.update!(end_date: 3.days.from_now)
      rate_plan.reload
      expect(rate_plan.status).to eq(RatePlan::Status::ACTIVE)
    end

    it 'changes end_date to DateTime.now if status has changed to retired' do
      rate_plan = subject.create!(
        title: 'New Title',
        description: 'New Description',
        end_date: nil,
        start_date: DateTime.now,
        recurrence: :one_time,
        product_id: product.id
      )

      expect(rate_plan.end_date).to be_nil
      rate_plan.update!(status: RatePlan::Status::RETIRED)
      rate_plan.reload
      expect(rate_plan.status).to eq(RatePlan::Status::RETIRED)
      expect(rate_plan.end_date).to_not be_nil
    end
  end
end
