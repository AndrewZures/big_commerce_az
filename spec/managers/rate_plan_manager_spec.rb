describe RatePlanManager do
  subject { described_class }
  let!(:product) { create(:product) }
  let!(:rate_plan1) do
    create(:rate_plan,
           title: 'rate_plan 1',
           description: 'description 1',
           product_id: product.id,
           recurrence: :one_time,
           start_date: DateTime.now)
  end
  let!(:rate_plan2) do
    create(:rate_plan,
           title: 'rate_plan 2',
           product_id: product.id,
           recurrence: :one_time,
           start_date: DateTime.now)
  end
  let!(:rate_plan3) do
    create(:rate_plan,
           title: 'rate_plan 3',
           product_id: product.id,
           recurrence: :one_time,
           start_date: DateTime.now)
  end

  def invalid_rate_plan_id
    RatePlan.maximum(:id).to_i.next
  end

  describe '#all' do
    it 'returns all rate_plans' do
      rate_plans = subject.all
      expect(rate_plans.count).to eq(3)
      expect(rate_plans.to_a).to include(rate_plan1, rate_plan2, rate_plan3)
    end
  end

  describe '#find' do
    it 'returns rate_plan instance if found' do
      found_rate_plan = subject.find(rate_plan2.id)
      expect(found_rate_plan).to eq(rate_plan2)
    end

    it 'returns nil if rate_plan cannot be found' do
      found_rate_plan = subject.find(invalid_rate_plan_id)
      expect(found_rate_plan).to eq(nil)
    end
  end

  describe '#new' do
    it 'returns a new rate_plan instance' do
      rate_plan = subject.new
      expect(rate_plan).to be_a(RatePlan)
    end
  end

  describe '#create' do
    context 'when valid rate_plan data' do
      it 'saves rate_plan and return true in response' do
        data = {
          product_id: product.id,
          title: 'rate_plan Title',
          description: 'rate_plan Description',
          recurrence: :one_time,
          start_date: DateTime.parse('2017/01/01')
        }
        saved, data = subject.create(data)
        expect(saved).to eq(true)

        created_rate_plan = RatePlan.last
        expect(data[:id]).to eq(created_rate_plan.id)
      end
    end

    context 'when extraneous data is added' do
      it 'filters out extra data and saves rate_plan' do
        data = {
          product_id: product.id,
          title: 'Rate Plan Title',
          description: 'Rate Plan Description',
          recurrence: :one_time,
          start_date: DateTime.parse('2017/01/01'),
          extra_field1: 'extra data 1',
          extra_field2: 'extra data 2'
        }
        saved, data = subject.create(data)
        expect(saved).to eq(true)

        created_rate_plan = RatePlan.last
        expect(data[:id]).to eq(created_rate_plan.id)
        expect(created_rate_plan.title).to eq('Rate Plan Title')
      end
    end

    context 'when invalid rate_plan data' do
      it 'returns false with errors when rate_plan data is invalid' do
        invalid_rate_plan = double(:invalid_rate_plan, valid?: false, errors: ['mock-error-message'])
        allow(RatePlan).to receive(:new).and_return(invalid_rate_plan)

        data = {
          title: 'Rate Plan Title',
          description: 'Rate Plan Description',
          recurrence: :one_time,
          start_date: DateTime.parse('2017/01/01')
        }

        saved, data = subject.create(data)
        expect(saved).to eq(false)
        # this should be unmocked once validations are added back in
        expect(data[:errors][:model]).to eq(invalid_rate_plan)
      end
    end
  end

  describe '#update' do
    it 'return false if rate_plan to update does not exist' do
      updates = { title: 'updated title' }
      updated, data = subject.update(invalid_rate_plan_id, updates)
      expect(updated).to eq(false)
      expect(data[:errors]).to eq({})
    end

    it 'updates an existing rate_plan' do
      expect(rate_plan1.title).to eq('rate_plan 1')
      expect(rate_plan1.description).to eq('description 1')

      updates = { title: 'updated title', description: 'updated description' }
      updated, data = subject.update(rate_plan1.id, updates)
      expect(updated).to eq(true)
      expect(data[:id]).to eq(rate_plan1.id)

      rate_plan1.reload
      expect(rate_plan1.title).to eq('updated title')
      expect(rate_plan1.description).to eq('updated description')
    end

    it 'filters out extra data and updates rate_plan' do
      updated_start_date = DateTime.parse('2017/01/01')
      data = {
        title: 'Rate Plan Title',
        description: 'Rate Plan Description',
        start_date: updated_start_date,
        extra_field1: 'extra data 1',
        extra_field2: 'extra data 2'
      }
      updated, data = subject.update(rate_plan1.id, data)
      expect(updated).to eq(true)
      expect(data[:id]).to eq(rate_plan1.id)

      rate_plan1.reload
      expect(rate_plan1.title).to eq('Rate Plan Title')
      expect(rate_plan1.description).to eq('Rate Plan Description')
      expect(rate_plan1.start_date).to eq(updated_start_date)
    end
  end

  describe '#delete' do
    it 'soft deletes rate_plan' do
      expect(rate_plan1.status).to eq('active')

      updated, _data = subject.delete(rate_plan1.id)
      expect(updated).to eq(true)

      rate_plan1.reload
      expect(rate_plan1.status).to eq(RatePlan::Status::RETIRED)
    end
  end
end
