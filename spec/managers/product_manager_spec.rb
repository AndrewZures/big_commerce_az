describe ProductManager do
  subject { described_class }
  let!(:product1) { create(:product, title: 'product 1', description: 'description 1') }
  let!(:product2) { create(:product, title: 'product 2', description: 'description 2') }
  let!(:product3) { create(:product, title: 'product 3', description: 'description 3') }

  def invalid_product_id
    Product.maximum(:id).to_i.next
  end

  describe '#all' do
    it 'returns all products' do
      products = subject.all
      expect(products.count).to eq(3)
      expect(products.to_a).to include(product1, product2, product3)
    end
  end

  describe '#find' do
    it 'returns product instance if found' do
      found_product = subject.find(product2.id)
      expect(found_product).to eq(product2)
    end

    it 'returns nil if product cannot be found' do
      found_product = subject.find(invalid_product_id)
      expect(found_product).to eq(nil)
    end
  end

  describe '#new' do
    it 'returns a new Product instance' do
      product = subject.new
      expect(product).to be_a(Product)
    end
  end

  describe '#create' do
    context 'when valid product data' do
      it 'saves product and return true in response' do
        data = {
          title: 'Product Title',
          description: 'Product Description',
          start_date: DateTime.parse('2017/01/01')
        }
        saved, data = subject.create(data)
        expect(saved).to eq(true)

        created_product = Product.last
        expect(data[:id]).to eq(created_product.id)
      end
    end

    context 'when extraneous data is added' do
      it 'filters out extra data and saves product' do
        data = {
          title: 'Product Title',
          description: 'Product Description',
          start_date: DateTime.parse('2017/01/01'),
          extra_field1: 'extra data 1',
          extra_field2: 'extra data 2'
        }
        saved, data = subject.create(data)
        expect(saved).to eq(true)

        created_product = Product.last
        expect(data[:id]).to eq(created_product.id)
        expect(created_product.title).to eq('Product Title')
      end
    end

    context 'when invalid product data' do
      it 'returns false with errors when product data is invalid' do
        invalid_product = double(:invalid_product, valid?: false, errors: ['mock-error-message'])
        allow(Product).to receive(:new).and_return(invalid_product)

        data = {
          title: 'Product Title',
          description: 'Product Description',
          start_date: DateTime.parse('2017/01/01')
        }

        saved, data = subject.create(data)
        expect(saved).to eq(false)
        # this should be unmocked once validations are added back in
        expect(data[:errors][:model]).to eq(invalid_product)
      end
    end
  end

  describe '#update' do
    it 'return false if product to update does not exist' do
      updates = { title: 'updated title' }
      updated, data = subject.update(invalid_product_id, updates)
      expect(updated).to eq(false)
      expect(data[:errors]).to eq({})
    end

    it 'updates an existing product' do
      expect(product1.title).to eq('product 1')
      expect(product1.description).to eq('description 1')

      updates = { title: 'updated title', description: 'updated description' }
      updated, data = subject.update(product1.id, updates)
      expect(updated).to eq(true)
      expect(data[:id]).to eq(product1.id)

      product1.reload
      expect(product1.title).to eq('updated title')
      expect(product1.description).to eq('updated description')
    end

    it 'filters out extra data and updates product' do
      updated_start_date = DateTime.parse('2017/01/01')
      data = {
        title: 'Product Title',
        description: 'Product Description',
        start_date: updated_start_date,
        extra_field1: 'extra data 1',
        extra_field2: 'extra data 2'
      }
      updated, data = subject.update(product1.id, data)
      expect(updated).to eq(true)
      expect(data[:id]).to eq(product1.id)

      product1.reload
      expect(product1.title).to eq('Product Title')
      expect(product1.description).to eq('Product Description')
      expect(product1.start_date).to eq(updated_start_date)
    end
  end

  describe '#delete' do
    it 'soft deletes product' do
      expect(product1.status).to eq(Product::Status::ACTIVE)

      updated, _data = subject.delete(product1.id)
      expect(updated).to eq(true)

      product1.reload
      expect(product1.status).to eq(Product::Status::DISCONTINUED)
    end
  end
end
