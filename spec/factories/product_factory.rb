FactoryGirl.define do
  factory :product do
    sequence(:title) { |n| "Product #{n}" }
    sequence(:description) { |n| "Product Description #{n}" }
    start_date { DateTime.now }
    end_date nil
  end
end
