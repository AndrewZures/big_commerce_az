FactoryGirl.define do
  factory :rate_plan do
    sequence(:title) { |n| "Rate Plan #{n}" }
    sequence(:description) { |n| "Rate Plan Description #{n}" }
    recurrence :one_time
    start_date { DateTime.now }
    end_date nil
  end
end
