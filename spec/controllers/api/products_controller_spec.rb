require 'rails_helper'

describe Api::ProductsController, type: :controller do
  let!(:product1) { create(:product) }
  let!(:product2) { create(:product) }
  let!(:product3) { create(:product, status: Product::Status::DISCONTINUED) }
  let!(:rate_plan1) { create(:rate_plan, product: product1) }
  let!(:rate_plan2) { create(:rate_plan, product: product1) }
  let!(:rate_plan3) { create(:rate_plan, product: product1, status: RatePlan::Status::RETIRED) }

  def format_date(date)
    return unless date
    if date.is_a?(String)
      date = DateTime.parse(date)
    end

    date.strftime('%m/%d/%Y')
  end

  describe '#index' do
    it 'returns all active products' do
      get :index
      response_body = JSON.parse(response.body)
      expect(response_body.length).to eq(2)

      expect(response_body[0]['id']).to eq(product1.id)
      expect(response_body[0]['title']).to eq(product1.title)
      expect(response_body[0]['description']).to eq(product1.description)
      expect(format_date(response_body[0]['start_date']))
        .to eq(format_date(product1.start_date))
      expect(format_date(response_body[0]['end_date']))
        .to eq(format_date(product1.end_date))

      expect(response_body[1]['id']).to eq(product2.id)
      expect(response_body[1]['title']).to eq(product2.title)
      expect(response_body[1]['description']).to eq(product2.description)
      expect(format_date(response_body[1]['start_date']))
        .to eq(format_date(product2.start_date))
      expect(format_date(response_body[1]['end_date']))
        .to eq(format_date(product2.end_date))
    end
  end

  describe '#show' do
    it 'returns product info' do
      get :show, params: { id: product1.id }
      response_body = JSON.parse(response.body)
      expect(response_body['id']).to eq(product1.id)
      expect(response_body['title']).to eq(product1.title)
      expect(response_body['rate_plans']).to_not be_nil
      expect(response_body['description']).to eq(product1.description)
      expect(format_date(response_body['start_date']))
        .to eq(format_date(product1.start_date))
      expect(format_date(response_body['end_date']))
        .to eq(format_date(product1.end_date))
    end

    it 'product info includes only active rate plans' do
      get :show, params: { id: product1.id }
      response_body = JSON.parse(response.body)
      expect(response_body['rate_plans'].count).to eq(2)

      expect(response_body['rate_plans'][0]['id']).to eq(rate_plan1.id)
      expect(response_body['rate_plans'][0]['title']).to eq(rate_plan1.title)
      expect(response_body['rate_plans'][0]['description']).to eq(rate_plan1.description)
      expect(response_body['rate_plans'][0]['recurrence']).to eq(rate_plan1.recurrence)
      expect(response_body['rate_plans'][0]['price'].to_f).to eq(rate_plan1.price.to_f)
      expect(format_date(response_body['rate_plans'][0]['start_date']))
        .to eq(format_date(rate_plan1.start_date))
      expect(format_date(response_body['rate_plans'][0]['end_date']))
        .to eq(format_date(rate_plan1.end_date))

      expect(response_body['rate_plans'][1]['id']).to eq(rate_plan2.id)
      expect(response_body['rate_plans'][1]['title']).to eq(rate_plan2.title)
      expect(response_body['rate_plans'][1]['description']).to eq(rate_plan2.description)
      expect(response_body['rate_plans'][1]['recurrence']).to eq(rate_plan2.recurrence)
      expect(response_body['rate_plans'][1]['price'].to_f).to eq(rate_plan2.price.to_f)
      expect(format_date(response_body['rate_plans'][1]['start_date']))
        .to eq(format_date(rate_plan2.start_date))
      expect(format_date(response_body['rate_plans'][1]['end_date']))
        .to eq(format_date(rate_plan2.end_date))
    end

    it 'returns error message if product could not be found' do
      get :show, params: { id: 12345 }
      response_body = JSON.parse(response.body)
      expect(response_body['error']).to_not be_empty
    end
  end
end
