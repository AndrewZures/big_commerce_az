require 'rails_helper'

describe Admin::RatePlansController, type: :controller do
  let!(:product) { create(:product) }
  let!(:rate_plan1) { create(:rate_plan, product: product, recurrence: :one_time, start_date: DateTime.now) }
  let!(:rate_plan2) { create(:rate_plan, product: product, recurrence: :one_time, start_date: DateTime.now) }
  let!(:rate_plan3) { create(:rate_plan, product: product, status: RatePlan::Status::RETIRED, recurrence: :one_time, start_date: DateTime.now) }

  describe '#index' do
    it 'returns all rate_plans' do
      get :index, params: { product_id: product.id }
      expect(assigns[:presentation_filters]).to eq({})
      expect(assigns[:product]).to eq(product)
      expect(assigns[:rate_plans]).to include(rate_plan1, rate_plan2, rate_plan3)
    end

    it 'filters status by rate_plan if status query param present' do
      get :index, params: { product_id: product.id, status: RatePlan::Status::ACTIVE }
      expect(assigns[:presentation_filters].to_hash).to eq('status' => RatePlan::Status::ACTIVE)
      expect(assigns[:product]).to eq(product)
      expect(assigns[:rate_plans]).to include(rate_plan1, rate_plan2)
    end
  end

  describe '#new' do
    it 'builds new rate_plan' do
      get :new, params: { product_id: product.id }
      expect(assigns[:rate_plan].kind_of?(RatePlan)).to eq(true)
      expect(assigns[:product]).to eq(product)
    end
  end

  describe '#create' do
    it 'saves rate_plan' do
      rate_plan_data = {
        title: 'New RatePlan',
        description: 'New RatePlan Description',
        start_date: DateTime.parse('07/07/2017'),
        end_date: DateTime.parse('08/08/2088'),
        recurrence: :one_time,
        product_id: product.id
      }
      post :create, params: { product_id: product.id, rate_plan: rate_plan_data }
      expect(response).to redirect_to(admin_product_rate_plans_path(product))

      created_rate_plan = RatePlan.last
      expect(created_rate_plan.title).to eq(rate_plan_data[:title])
      expect(created_rate_plan.description).to eq(rate_plan_data[:description])
      expect(created_rate_plan.start_date).to eq(rate_plan_data[:start_date])
      expect(created_rate_plan.end_date).to eq(rate_plan_data[:end_date])
      expect(created_rate_plan.product_id).to eq(rate_plan_data[:product_id])
    end

    it 'redirects back to new rate_plan path if error saving' do
      mock_full_messages = double(:errors, full_messages: [])
      mock_active_record_errors = double(:ar_error, errors: mock_full_messages)
      allow(RatePlanManager).to receive(:create).and_return([false, errors: { model: mock_active_record_errors }])
      original_rate_plan_count = RatePlan.count
      rate_plan_data = {
        title: 'New RatePlan',
        description: 'New RatePlan Description',
        start_date: DateTime.parse('07/07/2017'),
        end_date: DateTime.parse('08/08/2088'),
        product_id: product.id
      }
      post :create, params: { product_id: product.id, rate_plan: rate_plan_data }
      expect(response).to render_template(:new)
      expect(RatePlan.count).to eq(original_rate_plan_count)
    end
  end

  describe '#show' do
    it 'finds correct rate_plan' do
      get :show, params: { product_id: product.id, id: rate_plan1.id }
      expect(assigns[:rate_plan]).to eq(rate_plan1)
      expect(assigns[:product]).to eq(product)
    end
  end

  describe '#edit' do
    it 'finds correct rate_plan' do
      get :edit, params: { product_id: product.id, id: rate_plan2.id }
      expect(assigns[:rate_plan]).to eq(rate_plan2)
    end
  end

  describe '#update' do
    it 'correctly updates rate_plan' do
      rate_plan_data = {
        title: 'Updated RatePlan',
        description: 'Updated Description',
        start_date: DateTime.parse('07/07/2017'),
        end_date: DateTime.parse('08/08/2088'),
        product_id: product.id
      }
      put :update, params: { product_id: product.id, id: rate_plan1.id, rate_plan: rate_plan_data }
      expect(response).to redirect_to(admin_product_rate_plan_path(rate_plan1))

      rate_plan1.reload
      expect(rate_plan1.title).to eq(rate_plan_data[:title])
      expect(rate_plan1.description).to eq(rate_plan_data[:description])
      expect(rate_plan1.start_date).to eq(rate_plan_data[:start_date])
      expect(rate_plan1.end_date).to eq(rate_plan_data[:end_date])
      expect(rate_plan1.product_id).to eq(rate_plan_data[:product_id])
    end

    it 'redirects back to new rate_plan path if error saving' do
      mock_full_messages = double(:errors, full_messages: [])
      mock_active_record_errors = double(:ar_error, errors: mock_full_messages)
      allow(RatePlanManager).to receive(:update).and_return([false, errors: { model: mock_active_record_errors }])
      original_rate_plan_count = RatePlan.count
      rate_plan_data = {
        title: 'New RatePlan',
        description: 'New RatePlan Description',
        start_date: DateTime.parse('07/07/2017'),
        end_date: DateTime.parse('08/08/2088'),
        product_id: product.id
      }
      post :update, params: { product_id: product.id, id: rate_plan1.id, rate_plan: rate_plan_data }
      expect(response).to render_template(:edit)
    end
  end
end
