require 'rails_helper'

describe Admin::ProductsController, type: :controller do
  let(:product1) { create(:product) }
  let(:product2) { create(:product) }
  let(:product3) { create(:product, status: Product::Status::DISCONTINUED) }

  describe '#index' do
    it 'returns all products' do
      get :index
      expect(assigns[:filters]).to eq({})
      expect(assigns[:products]).to include(product1, product2, product3)
    end

    it 'filters status by product if status query param present' do
      get :index, params: { status: Product::Status::ACTIVE }
      expect(assigns[:filters]).to eq(status: Product::Status::ACTIVE.to_sym)
      expect(assigns[:products]).to include(product1, product2)
    end
  end

  describe '#new' do
    it 'builds new product' do
      get :new
      expect(assigns[:product].kind_of?(Product)).to eq(true)
    end
  end

  describe '#create' do
    it 'saves product' do
      product_data = {
        title: 'New Product',
        description: 'New Product Description',
        start_date: DateTime.parse('07/07/2017'),
        end_date: DateTime.parse('08/08/2088')
      }
      post :create, params: { product: product_data }
      expect(response).to redirect_to(admin_products_path)

      created_product = Product.last
      expect(created_product.title).to eq(product_data[:title])
      expect(created_product.description).to eq(product_data[:description])
      expect(created_product.start_date).to eq(product_data[:start_date])
      expect(created_product.end_date).to eq(product_data[:end_date])
    end

    it 'redirects back to new product path if error saving' do
      mock_full_messages = double(:errors, full_messages: [])
      mock_active_record_errors = double(:ar_error, errors: mock_full_messages)
      allow(ProductManager).to receive(:create).and_return([false, errors: { model: mock_active_record_errors }])
      original_product_count = Product.count
      product_data = {
        title: 'New Product',
        description: 'New Product Description',
        start_date: DateTime.parse('07/07/2017'),
        end_date: DateTime.parse('08/08/2088')
      }
      post :create, params: { product: product_data }
      expect(response).to render_template(:new)
      expect(Product.count).to eq(original_product_count)
    end
  end

  describe '#show' do
    it 'finds correct product' do
      get :show, params: { id: product1.id }
      expect(assigns[:product]).to eq(product1)
    end
  end

  describe '#edit' do
    it 'finds correct product' do
      get :edit, params: { id: product2.id }
      expect(assigns[:product]).to eq(product2)
    end
  end

  describe '#update' do
    it 'correctly updates product' do
      product_data = {
        title: 'Updated Product',
        description: 'Updated Description',
        start_date: DateTime.parse('07/07/2017'),
        end_date: DateTime.parse('08/08/2088')
      }
      put :update, params: { id: product1.id, product: product_data }
      expect(response).to redirect_to(admin_product_path(product1))

      product1.reload
      expect(product1.title).to eq(product_data[:title])
      expect(product1.description).to eq(product_data[:description])
      expect(product1.start_date).to eq(product_data[:start_date])
      expect(product1.end_date).to eq(product_data[:end_date])
    end

    it 'redirects back to new product path if error saving' do
      mock_full_messages = double(:errors, full_messages: [])
      mock_active_record_errors = double(:ar_error, errors: mock_full_messages)
      allow(ProductManager).to receive(:update).and_return([false, errors: { model: mock_active_record_errors}])
      original_product_count = Product.count
      product_data = {
        title: 'New Product',
        description: 'New Product Description',
        start_date: DateTime.parse('07/07/2017'),
        end_date: DateTime.parse('08/08/2088')
      }
      post :update, params: { id: product1.id, product: product_data }
      expect(response).to render_template(:edit)
    end
  end
end
