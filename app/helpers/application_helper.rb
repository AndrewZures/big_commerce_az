module ApplicationHelper
  def format_datetime(datetime_object)
    return unless datetime_object
    datetime_object.strftime('%m/%d/%Y')
  end
end
