class Product < ActiveRecord::Base
  include ActiveModel::Dirty
  validates :start_date, presence: true
  validates :title, presence: true
  validates :status, presence: true

  has_many :rate_plans
  before_save :sync_fields

  class Status
    ACTIVE = 'active'
    DISCONTINUED = 'discontinued'
    ALL_STATUSES = [ACTIVE, DISCONTINUED]
  end

  def self.editable_fields
    %I(title description start_date end_date status)
  end

  def sync_fields
    if end_date_changed?
      discontinued = !end_date.nil? && (end_date < DateTime.now)
      self.status = discontinued ? Status::DISCONTINUED : Status::ACTIVE
    elsif status_changed?
      case status
      when Status::ACTIVE
        self.end_date = nil
      when Status::DISCONTINUED
        self.end_date = DateTime.now
      end
    end
  end
end
