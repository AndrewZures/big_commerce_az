class RatePlan < ActiveRecord::Base
  include ActiveModel::Dirty
  validates :title, presence: true
  validates :status, presence: true
  validates :recurrence, presence: true
  validates :price, presence: true
  validates :start_date, presence: true

  belongs_to :product

  before_save :sync_fields

  enum recurrence: { one_time: 0, monthly: 1, quarterly: 2, annually: 3 }

  class Status
    ACTIVE = 'active'
    RETIRED = 'retired'
    ALL_STATUSES = [ACTIVE, RETIRED]
  end

  def self.editable_fields
    %I[title description start_date end_date product_id recurrence price status]
  end

  def sync_fields
    if end_date_changed?
      retired = !end_date.nil? && (end_date < DateTime.now)
      self.status = retired ? Status::RETIRED : Status::ACTIVE
    elsif status_changed?
      case status
      when Status::ACTIVE
        self.end_date = nil
      when Status::RETIRED
        self.end_date = DateTime.now
      end
    end
  end
end
