module Admin
  class RatePlansController < ApplicationController
    def index
      permitted = params.permit([:status, :product_id])

      filter_keys = [:status, :product_id]
      data_filters = permitted.slice(*filter_keys)
      @rate_plans = RatePlanManager.all(data_filters)
      @product = ProductManager.find(permitted[:product_id])
      @presentation_filters = data_filters.except(:product_id)
    end

    def new
      @product = ProductManager.find(params['product_id'])
      @rate_plan = RatePlanManager.new(product_id: @product.id, start_date: DateTime.now)
    end

    def create
      permitted = params.require(:rate_plan).permit(RatePlanManager.permitted_values)
      created, data = RatePlanManager.create(permitted)
      if created
        flash[:notice] = 'Rate Plan Created'
        redirect_to admin_product_rate_plans_path(permitted[:product_id])
      else
        @rate_plan = data[:errors][:model]
        @product = ProductManager.find(params[:product_id])
        listed_error_message = @rate_plan.errors.full_messages.join('\n')
        flash[:error] = "Rate Plan Creation Error:\n #{listed_error_message}"
        render :new
      end
    end

    def show
      @rate_plan = RatePlanManager.find(params[:id])
      @product = ProductManager.find(@rate_plan.product_id)
    end

    def edit
      @rate_plan = RatePlanManager.find(params[:id])
    end

    def update
      permitted = params.require(:rate_plan).permit(RatePlanManager.permitted_values)
      updated, data = RatePlanManager.update(params[:id], permitted)
      if updated
        flash[:notice] = 'Rate Plan Updated'
        redirect_to admin_product_rate_plan_path(params[:id])
      else
        @rate_plan = data[:errors][:model]
        @product = ProductManager.find(params[:product_id])
        listed_error_message = @rate_plan.errors.full_messages.join('/\n')
        flash[:error] = "Rate Plan Update Error:\n #{listed_error_message}"
        render :edit
      end
    end
  end
end
