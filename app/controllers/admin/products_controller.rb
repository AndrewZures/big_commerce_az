module Admin
  class ProductsController < ApplicationController
    def index
      @filters = params[:status] ? { status: params[:status].downcase.to_sym } : {}
      @products = ProductManager.all(@filters)
    end

    def new
      @product = ProductManager.new(start_date: DateTime.now)
    end

    def create
      permitted = params.require(:product).permit(ProductManager.permitted_values)
      created, data = ProductManager.create(permitted)
      if created
        flash[:notice] = 'Product Created'
        redirect_to admin_products_path
      else
        @product = data[:errors][:model]
        listed_error_message = @product.errors.full_messages.join('\n')
        flash[:error] = "Product Creation Error:\n #{listed_error_message}"
        render :new
      end
    end

    def show
      @product = ProductManager.find(params[:id])
    end

    def edit
      @product = ProductManager.find(params[:id])
    end

    def update
      permitted = params.require(:product).permit(ProductManager.permitted_values)
      updated, data = ProductManager.update(params[:id], permitted)

      if updated
        flash[:notice] = 'Product Updated'
        redirect_to admin_product_path(params[:id])
      else
        @product = data[:errors][:model]
        listed_error_message = @product.errors.full_messages.join('/\n')
        flash[:error] = "Product Update Error:\n #{listed_error_message}"
        render :edit
      end
    end
  end
end
