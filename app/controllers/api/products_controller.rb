module Api
  class ProductsController < ApplicationController
    def index
      products = ProductManager.all(status: Product::Status::ACTIVE)
      render json: products
    end

    def show
      product = ProductManager.find(params[:id])
      rate_plans = RatePlan.where(product_id: params[:id], status: RatePlan::Status::ACTIVE)
      if product
        product_data = product.as_json
        product_data['rate_plans'] = rate_plans.as_json
        render json: product_data
      else
        render json: { error: 'Product Not Found' }
      end
    end
  end
end
