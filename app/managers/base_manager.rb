class BaseManager
  def self.permitted_values
    model_class.editable_fields
  end

  def self.all(filters = {})
    filters.empty? ? model_class.all : model_class.where(filters)
  end

  def self.new(*args)
    model_class.new(*args)
  end

  def self.create(data)
    clean_data = clean_data(data)
    clean_data['start_date'] ||= DateTime.now
    model = model_class.new(clean_data)
    if model.valid?
      model.save!
      [true, id: model.id]
    else
      # should return errors list at a minimum
      [false, errors: { model: model }]
    end
  end

  def self.find(id)
    model_class.find_by(id: id)
  end

  def self.update(id, updates)
    return unless id
    clean_updates = clean_data(updates)
    model = find(id)
    return [false, errors: {}] unless model
    model.attributes = clean_updates

    if model.valid?
      model.save!
      [true, id: model.id]
    else
      [false, errors: { model: model }]
    end
  end

  def self.delete(id)
    update(id, end_date: DateTime.now)
  end

  def self.clean_data(data = {})
    stringified_data = data.stringify_keys
    stringified_data.slice(*model_class.column_names)
  end
end
