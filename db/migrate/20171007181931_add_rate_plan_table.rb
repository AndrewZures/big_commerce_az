class AddRatePlanTable < ActiveRecord::Migration[5.1]
  def change
    create_table(:rate_plans) do |t|
      t.column :product_id, :integer, null: false
      t.column :title, :string, null: false
      t.column :description, :text
      t.column :start_date, :datetime, null: false
      t.column :end_date, :datetime
      t.column :recurrence, :integer, null: false
      t.column :price, :decimal, precision: 10, scale: 2, default: 0.00
      t.column :status, :string, default: 'active'
    end
  end
end
