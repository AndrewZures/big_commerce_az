class AddProductTable < ActiveRecord::Migration[5.1]
  def change
    create_table(:products) do |t|
      t.column :title, :string, null: false
      t.column :description, :text
      t.column :start_date, :datetime, null: false
      t.column :end_date, :datetime
      t.column :status, :string, default: 'active'
    end
  end
end
