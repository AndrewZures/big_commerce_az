# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#

products = Product.create([
  { title: 'Product 1', description: 'Description 1', start_date: DateTime.now },
  { title: 'Product 2', description: 'Description 2', start_date: DateTime.now },
  { title: 'Product 3', description: 'Description 3', start_date: DateTime.now },
  { title: 'Product 4', description: 'Description 4', start_date: DateTime.now },
  { title: 'Product 5', description: 'Description 5', start_date: DateTime.now },
  { title: 'Product 6', description: 'Description 6', start_date: DateTime.now }
])

rate_plans = RatePlan.create([
  { title: 'Rate Plan 1', description: 'RP Description 1', start_date: DateTime.now, recurrence: 0, product_id: 1, price: 20.0 },
  { title: 'Rate Plan 2', description: 'RP Description 2', start_date: DateTime.now, recurrence: 1, product_id: 1, price: 35.2 },
  { title: 'Rate Plan 3', description: 'RP Description 3', start_date: DateTime.now, recurrence: 1, product_id: 1, price: 200.0 },
  { title: 'Rate Plan 4', description: 'RP Description 4', start_date: DateTime.now, recurrence: 2, product_id: 2, price: 410.05 },
  { title: 'Rate Plan 5', description: 'RP Description 5', start_date: DateTime.now, recurrence: 3, product_id: 3, price: 1000.01 },
  { title: 'Rate Plan 6', description: 'RP Description 6', start_date: DateTime.now, recurrence: 0, product_id: 3, price: 21.0 }
])
